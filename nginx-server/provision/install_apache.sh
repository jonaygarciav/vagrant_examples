#!/usr/bin/env bash
#title      : install_apache.sh
#description: This script will install NGINX server on Ubuntu System
#author     : Jonay Garcia <jonaygarciav@gmail.com>
#date       : 28/09/2018
#version    : 1.0
#usage      : ./install_apache.sh
#==============================================================================

# Update system and install NGINX server
apt-get update
apt-get install -y apache2

# Copy bootstrap.html to root directory
cp /vagrant/provision/bootstrap4.html /var/www/html/

# Change owner for root directory
#    * www-data: user who runs the NGINX service on the system
#    * /var/www/html: default root folder of the web server
chown -R www-data /var/www/html