#!/usr/bin/env bash
#title      : install_nginx.sh
#description: This script will install NGINX server on Ubuntu System
#author     : Jonay Garcia <jonaygarciav@gmail.com>
#date       : 28/09/2018
#version    : 1.0
#usage      : ./install_nginx.sh
#==============================================================================

# Update system and install NGINX server
apt-get update
apt-get install -y nginx

# Move bootstrap.html to root directory
#mv /tmp/bootstrap4.html /var/www/html/
#cp /vagrant/provision/bootstrap4.html /var/www/html/
cp /vagrant/bootstrap4.html /var/www/html/

# Change owner for root directory
#    * www-data: user who runs the NGINX service on the system
#    * /var/www/html: default root folder of the web server
chown -R www-data /var/www/html


